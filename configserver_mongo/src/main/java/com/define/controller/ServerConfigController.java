package com.define.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.define.cloud.basedependency.utils.JsonUtil;
import com.define.common.CommonUtil;
import com.define.entity.AreaConfig;
import com.define.service.AreaConfigService;
import com.define.service.ServerConfigService;
import com.mongodb.client.gridfs.model.GridFSFile;

@RestController
public class ServerConfigController {

	@Autowired
	ServerConfigService serverService;

	@RequestMapping(value = "/findServerVersion")
	public Map<String, Object> updateAreaFile(String source, HttpServletRequest req) {
		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			String version = (String) map.get("version");
			String areaId = (String) map.get("areaId");
			String serverId = (String) map.get("serverId");
			String last_version = serverService.getVersionByLast_time(areaId, serverId);
			Map<String, Object> returnMap = CommonUtil.compareVersion(last_version, version, req, "getServerConfig",
					"addServerConfig");
			return returnMap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return JsonUtil.outMap("-1", "版本信息错误");

	}

	/**
	 * 做备份or手动更新文件上传
	 * 
	 * @param file
	 * @param source {performId}
	 * @param req
	 */
	@RequestMapping(value = "/addServerConfig", method = RequestMethod.POST)
	public void addConfigFile(@RequestParam(value = "file") MultipartFile file, String source, HttpServletRequest req) {
		serverService.backUpFile(file, source);
	}

	/**
	 * 按时间下载最新配置文件
	 * 
	 * @param file
	 * @param source {performId}
	 * @param req
	 */
	@RequestMapping(value = "/getServerConfig", method = RequestMethod.POST)
	public void downLoadConfigFile(String source, HttpServletResponse rep) {
		InputStream stream = serverService.downLoadConfigFile(source, rep);
		if (null != stream) {
			try {
				byte[] buffer = CommonUtil.readInputStream(stream);
				ServletOutputStream out = rep.getOutputStream();
				out.write(buffer);
				out.flush();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
