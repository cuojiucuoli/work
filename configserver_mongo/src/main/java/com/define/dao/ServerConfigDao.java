package com.define.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.define.entity.ServerConfig;

public interface ServerConfigDao extends MongoRepository<ServerConfig, String>{

}
