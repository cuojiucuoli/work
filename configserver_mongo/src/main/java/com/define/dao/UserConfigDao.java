package com.define.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.define.entity.UserConfig;

public interface UserConfigDao extends MongoRepository<UserConfig,String>{

}
