package com.define.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.define.entity.AreaConfig;
import com.define.entity.DeviceConfig;

public interface DeviceConfigDao extends MongoRepository<DeviceConfig,String>{

}
