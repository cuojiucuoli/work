package com.define.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.define.entity.AreaConfig;

public interface AreaConfigDao extends MongoRepository<AreaConfig,String>{

}
