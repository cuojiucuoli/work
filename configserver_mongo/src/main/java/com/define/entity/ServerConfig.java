package com.define.entity;

import org.springframework.data.annotation.Id;

public class ServerConfig {

	@Id
	private String id;

	private String version;

	private String path;

	private Long latestTime;

	private String areaId;//湛江

	private String serverId;//001

	private String configType;//设备配置服务
	
	private String fileName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getLatestTime() {
		return latestTime;
	}

	public void setLatestTime(Long latestTime) {
		this.latestTime = latestTime;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getConfigType() {
		return configType;
	}

	public void setConfigType(String configType) {
		this.configType = configType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
