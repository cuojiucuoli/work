package com.define.entity;

import org.springframework.data.annotation.Id;

public class DeviceConfig {
	@Id
	private String id;

	private String version;

	private String path;

	private Long latestTime;

	private String areaId;//湛江

	private String itemCode;//001

	private String configType;//设备配置服务
	
	private String fileName;
	
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getConfigType() {
		return configType;
	}
	public void setConfigType(String configType) {
		this.configType = configType;
	}
	public Long getLatestTime() {
		return latestTime;
	}
	public void setLatestTime(Long latestTime) {
		this.latestTime = latestTime;
	}
	@Override
	public String toString() {
		return "DeviceConfig [id=" + id + ", version=" + version + ", path=" + path + ", latestTime=" + latestTime
				+ ", areaId=" + areaId + ", itemCode=" + itemCode + ", configType=" + configType + "]";
	}

	
	
}
