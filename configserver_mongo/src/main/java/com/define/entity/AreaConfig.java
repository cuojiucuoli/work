package com.define.entity;

import org.springframework.data.annotation.Id;

public class AreaConfig {
	@Id
	private String id; //区域配置文件id

	private String areaId;//区域id

	private Long latestTime;

	private String platformId;

	private String path;

	private String configType;
	
	private String fileName;

	private String version;
	
	
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public Long getLatestTime() {
		return latestTime;
	}
	public void setLatestTime(Long latestTime) {
		this.latestTime = latestTime;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getConfigType() {
		return configType;
	}
	public void setConfigType(String configType) {
		this.configType = configType;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	
	
}
