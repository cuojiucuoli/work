package com.define.common;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.define.cloud.basedependency.utils.JsonUtil;
import com.define.controller.AreaConfigController;

public class CommonUtil {

	public static Map<String, Object> compareVersion(String last_version, String version, HttpServletRequest req,
			String downloadUri, String uploadUri) {
		if (StringUtils.isNotEmpty(last_version) && StringUtils.isNotEmpty(version)) {
			if (version.equals(last_version)) {
				Map<String, Object> outMap = JsonUtil.outMap("1", "已经是最新");
				return outMap;
			} else if (Integer.parseInt(last_version) > Integer.parseInt(version)) {
				Map<String, Object> resultMap = new HashMap<String, Object>();
				String url = "http://" + req.getServerName() + ":" + req.getServerPort() + "/" + downloadUri;
				resultMap.put("goto", url);
				resultMap.put("-1", "需要更新最新配置文件");
				return resultMap;
			} else if (Integer.parseInt(last_version) < Integer.parseInt(version)) {
				Map<String, Object> resultMap = new HashMap<String, Object>();
				String url = "http://" + req.getServerName() + ":" + req.getServerPort() + "/" + uploadUri;
				resultMap.put("goto", url);
				resultMap.put("-1", "数据文件备份");
				return resultMap;
			}
		}
		return JsonUtil.outMap("-1", "版本信息错误");

	}

	public static String getUploadPath(MultipartFile file, String source, HttpServletRequest req,
			Class<? extends AreaConfigController> clazz) {
				
				return source;
		
	}
	
	public static byte[] readInputStream(InputStream fis) throws IOException {
		 ByteArrayOutputStream outStream = new ByteArrayOutputStream();  
	        byte[] buffer = new byte[1024];  
	        int len = 0;  
	        while( (len=fis.read(buffer)) != -1 ){  
	            outStream.write(buffer, 0, len);  
	        }  
	        fis.close();  
	        return outStream.toByteArray();
	}
}
