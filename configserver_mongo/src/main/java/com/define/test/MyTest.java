package com.define.test;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.define.entity.AreaConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyTest {

	@Autowired
	MongoTemplate template;
	
	@Test
	public void HAHA() {
		AreaConfig area = new AreaConfig();
		area.setAreaId("002");
		area.setConfigType("区域配置");
		area.setId(UUID.randomUUID().toString());
		area.setLatestTime(System.currentTimeMillis());
		area.setPlatformId("002");
		area.setVersion("002");
		template.insert(area);
	}
}
