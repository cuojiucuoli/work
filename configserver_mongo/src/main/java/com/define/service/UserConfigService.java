package com.define.service;

import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

public interface UserConfigService {

	public String getVersionByLast_time(String areaName, String platformId);

	public void backUpFile(MultipartFile file, String source);

	public InputStream downLoadConfigFile(String source, HttpServletResponse rep);
}
