package com.define.service.imp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.define.cloud.basedependency.utils.JsonUtil;
import com.define.common.CommonUtil;
import com.define.dao.AreaConfigDao;
import com.define.entity.AreaConfig;
import com.define.service.AreaConfigService;

@Service
public class AreaServiceImp implements AreaConfigService {

	@Autowired
	AreaConfigDao areaDao;

	@Autowired
	MongoTemplate template;

	@Autowired
	GridFsTemplate fsTemplate;

	@Override
	public String getVersionByLast_time(String areaId, String platformId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("areaId").is(areaId).where("platformId").is(platformId));
		query.with(Sort.by(Order.asc("latestTime")));
		query.limit(1);
		AreaConfig areaConfig = template.findOne(query, AreaConfig.class);

		return areaConfig.getVersion();
	}

	@Override
	public void backUpFile(MultipartFile file, String source) {
		AreaConfig areaConfig = JsonUtil.jsonToObject(source, AreaConfig.class);
		String uuid = UUID.randomUUID().toString();
		areaConfig.setId(uuid);
		areaConfig.setLatestTime(System.currentTimeMillis());
		areaConfig.setFileName(file.getOriginalFilename());
		template.insert(areaConfig);

		InputStream inputStream = null;
		try {
			inputStream = file.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String originalFilename = uuid;
		String contentType = file.getContentType();
		fsTemplate.store(inputStream, originalFilename, contentType);
	}

	@Override
	public InputStream downLoadConfigFile(String source, HttpServletResponse rep) {
		InputStream inputStream = null;
		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			String areaId = (String) map.get("areaId");
			String platformId = (String) map.get("platformId");
			Query query = new Query();
			query.addCriteria(Criteria.where("areaId").is(areaId).where("platformId").is(platformId));
			query.with(Sort.by(Order.desc("version")));
			AreaConfig areaConfig = template.findOne(query, AreaConfig.class);
			String id = areaConfig.getId();
			
			GridFsResource resource = fsTemplate.getResource(id);
			inputStream = resource.getInputStream();
			if (inputStream != null) {		
				rep.reset();
				rep.addHeader("Content-Disposition", "attachment;filename=" + areaConfig.getFileName());
				rep.setContentType("application/octet-stream");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inputStream;
	}

}
