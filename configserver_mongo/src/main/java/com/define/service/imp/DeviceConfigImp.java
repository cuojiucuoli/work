package com.define.service.imp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.define.cloud.basedependency.utils.JsonUtil;
import com.define.entity.AreaConfig;
import com.define.entity.DeviceConfig;
import com.define.service.DeviceConfigService;

@Service
public class DeviceConfigImp implements DeviceConfigService {

	@Autowired
	MongoTemplate template;

	@Autowired
	GridFsTemplate fsTemplate;

	@Override
	public String getVersionByLast_time(String areaId, String itemCode) {
		Query query = new Query();
		query.addCriteria(Criteria.where("areaId").is(areaId).where("itemCode").is(itemCode));
		query.with(Sort.by(Order.asc("latestTime")));
		query.limit(1);
		DeviceConfig deviceConfig = template.findOne(query, DeviceConfig.class);

		return deviceConfig.getVersion();

	}

	@Override
	public void backUpFile(MultipartFile file, String source) {
		DeviceConfig deviceConfig = JsonUtil.jsonToObject(source, DeviceConfig.class);
		String uuid = UUID.randomUUID().toString();
		deviceConfig.setId(uuid);
		deviceConfig.setLatestTime(System.currentTimeMillis());
		deviceConfig.setFileName(file.getOriginalFilename());
		template.insert(deviceConfig);

		InputStream inputStream = null;
		try {
			inputStream = file.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String originalFilename = uuid;
		String contentType = file.getContentType();
		fsTemplate.store(inputStream, originalFilename, contentType);

	}

	@Override
	public InputStream downLoadConfigFile(String source, HttpServletResponse rep) {

		InputStream inputStream = null;
		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			String areaId = (String) map.get("areaId");
			String itemCode = (String) map.get("itemCode");
			Query query = new Query();
			query.addCriteria(Criteria.where("areaId").is(areaId).where("itemCode").is(itemCode));
			query.with(Sort.by(Order.desc("version")));
			DeviceConfig deviceConfig = template.findOne(query, DeviceConfig.class);
			String id = deviceConfig.getId();

			GridFsResource resource = fsTemplate.getResource(id);
			inputStream = resource.getInputStream();
			if (inputStream != null) {
				rep.reset();
				rep.addHeader("Content-Disposition", "attachment;filename=" + deviceConfig.getFileName());
				rep.setContentType("application/octet-stream");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inputStream;

	}

}
