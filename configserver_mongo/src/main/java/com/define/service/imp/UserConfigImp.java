package com.define.service.imp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.define.cloud.basedependency.utils.JsonUtil;
import com.define.entity.DeviceConfig;
import com.define.entity.UserConfig;
import com.define.service.UserConfigService;

public class UserConfigImp implements UserConfigService {

	@Autowired
	MongoTemplate template;

	@Autowired
	GridFsTemplate fsTemplate;

	@Override
	public String getVersionByLast_time(String areaId, String userId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("areaId").is(areaId).where("userId").is(userId));
		query.with(Sort.by(Order.asc("latestTime")));
		query.limit(1);
		UserConfig userConfig = template.findOne(query, UserConfig.class);

		return userConfig.getVersion();

	}

	@Override
	public void backUpFile(MultipartFile file, String source) {

		UserConfig userConfig = JsonUtil.jsonToObject(source, UserConfig.class);
		String uuid = UUID.randomUUID().toString();
		userConfig.setId(uuid);
		userConfig.setLatestTime(System.currentTimeMillis());
		userConfig.setFileName(file.getOriginalFilename());
		template.insert(userConfig);

		InputStream inputStream = null;
		try {
			inputStream = file.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String originalFilename = uuid;
		String contentType = file.getContentType();
		fsTemplate.store(inputStream, originalFilename, contentType);

	}

	@Override
	public InputStream downLoadConfigFile(String source, HttpServletResponse rep) {


		InputStream inputStream = null;
		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			String areaId = (String) map.get("areaId");
			String userId = (String) map.get("userId");
			Query query = new Query();
			query.addCriteria(Criteria.where("areaId").is(areaId).where("userId").is(userId));
			query.with(Sort.by(Order.desc("version")));
			UserConfig userConfig = template.findOne(query, UserConfig.class);
			String id = userConfig.getId();

			GridFsResource resource = fsTemplate.getResource(id);
			inputStream = resource.getInputStream();
			if (inputStream != null) {
				rep.reset();
				rep.addHeader("Content-Disposition", "attachment;filename=" + userConfig.getFileName());
				rep.setContentType("application/octet-stream");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return inputStream;

	
	}

}
