package com.define.service;

import java.util.List;

import com.define.entity.User;

public interface UserService {

	public List<User> findUserByIdentity(String identity);
	
	public void saveUser(User user);
	
	public User findUserByTradeId(String tradeId);
	
	public User findUserByIdentityOrderByCurrentTimeAsc(String Identity);
}
