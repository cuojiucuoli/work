package com.define.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.define.dao.UserDao;
import com.define.entity.User;
import com.define.service.UserService;
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserDao userDao;
	
	@Override
	public List<User> findUserByIdentity(String identity) {
		List<User> user = userDao.findUserByIdentity(identity);
		if(user.size()==0||null==user) {
			return null;
		}
		return user;
	}

	@Override
	public void saveUser(User user) {
		 userDao.save(user);
		
	}
	
	/**
	 * 给Job使用
	 */
	@Override
	public User findUserByTradeId(String tradeId) {
		User user = userDao.findUserByTradeId(tradeId);
		return user;
	}

	@Override
	public User findUserByIdentityOrderByCurrentTimeAsc(String Identity) {
		User user = userDao.findUserByIdentityOrderByCurTimeAsc(Identity);
		return user;
	}

}
