package com.define.common;

import java.util.Random;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import com.define.entity.User;
import com.define.job.OrderJob;
import com.define.service.UserService;
@Component
public class JobUtil {
	/*@Autowired
	private  Scheduler scheduler;*/

	@Autowired
	SchedulerFactoryBean  factoryBean;
	
	public  void invokeJob(String tradeId,int minute) {	
		System.out.println(Thread.currentThread().getName());
		
		Scheduler scheduler = factoryBean.getScheduler();
		try {
			Random random = new Random();
			
			/*scheduler = StdSchedulerFactory.getDefaultScheduler();*/

			JobDetail job = JobBuilder.newJob(OrderJob.class).withIdentity(tradeId,tradeId).usingJobData("tradeId", tradeId)
					.build();
			Trigger myTrigger = null;

			myTrigger = new CronTriggerImpl(Thread.currentThread().getName(), Scheduler.DEFAULT_GROUP,
					"59/59 1/1 * * * ? *");//   */20 * * * * ?     0 */"+minute+" * * * 

			scheduler.scheduleJob(job, myTrigger);
			
			scheduler.start();
		} catch (Exception e) {

		}

	}
}
