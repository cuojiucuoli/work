package com.define.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.DigestUtils;

import com.alibaba.fastjson.JSONObject;
import com.define.exception.StatusCodesExeption;

public class CommonUtil {

	private static Logger log = LoggerFactory.getLogger(CommonUtil.class);

	public static String generateSign(Map map) {

		String jsonStr = JSONObject.toJSONString(map);
		System.out.println(jsonStr);
		String password = "e10adc3949ba59abbe56e057f20f883e";
		String raw_password = password + jsonStr;
		try {
			byte[] bytes = raw_password.getBytes("utf-8");
			password = DigestUtils.md5DigestAsHex(bytes).toLowerCase();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return password;
	}

	public static String HttpClients(String method, String sign, Map map)
			throws Exception {
		CloseableHttpClient httpClient = org.apache.http.impl.client.HttpClients.createDefault();
		HttpPost mj = new HttpPost(
				"https://lwx.navboy.com/api/sale/0855a353-01e1/" + method + "?account=open~op&sign=" + sign);
		mj.setHeader("Content-Type", "application/json");
		mj.setHeader("charset", "charset=utf-8");
		/*
		 * Map<String,Object> maps = new HashMap<String,Object>(); Set entrySet =
		 * map.entrySet(); for (Object object : entrySet) { Entry<String,Object> entry =
		 * (Entry<String,Object>)object; maps.put(entry.getKey(), entry.getValue()); }
		 */
		Set entrySet = map.entrySet();
		for (Object object : entrySet) {
			Entry<String, Object> entry = (Entry<String, Object>) object;
			map.put(entry.getKey(), entry.getValue());
		}
		String js = JSONObject.toJSONString(map);
	
		StringEntity se = new StringEntity(js, "utf-8");
		se.setContentType("text/json;charset=utf-8");
		mj.setEntity(se);
		HttpResponse response;
		try {
			response = httpClient.execute(mj);
		} catch (Exception e) {
			throw e;
		}
		/*
		 * int code = response.getStatusLine().getStatusCode(); if(code>=400&&code<500)
		 * { log.error("获取第三方请求失败"); throw new StatusCodesExeption("获取第三方请求失败"); }
		 * if(code>=500&&code<600) { log.error("服务器内部发送了点错误,请稍后尝试"); throw new
		 * StatusCodesExeption("服务器内部发送了点错误,请稍后尝试"); }
		 */
		HttpEntity entity = response.getEntity();
		String jsonStr = EntityUtils.toString(entity);
		return jsonStr;
	}

	public static Map<String, Object> convertMap(Map<String, Object> maps) {
		Map<String, Object> newMap = new HashMap<String, Object>();
		Set<Entry<String, Object>> entrySet = maps.entrySet();
		for (Entry<String, Object> entry : entrySet) {
			newMap.put(entry.getKey(), entry.getValue());
		}
		return newMap;
	}

	public static void main(String[] args) {
		Map map = new HashMap();
		map.put("start", "");
		String generateSign = generateSign(map);
		System.out.println(generateSign);
	}
}
