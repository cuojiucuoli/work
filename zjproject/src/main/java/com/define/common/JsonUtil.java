package com.define.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.json.JSONObject;

public class JsonUtil {
	
	private  static ObjectMapper objectMapper = new ObjectMapper();

	protected static Log logger = LogFactory.getLog(JsonUtil.class);
    
    public static <T> Map<String,T>jsontoMap(String jsonstring)throws Exception{
    	ObjectMapper mapper = new ObjectMapper();
    	return mapper.readValue(jsonstring,new TypeReference<Map<String, T>>(){});
    }
    
    /**
     * 将json转换成对象Class
     * @param src
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T>T jsonToObject(String src,Class<T> clazz){
        if(StringUtils.isEmpty(src) || clazz == null){
            return null;
        }
        try {
            return clazz.equals(String.class) ? (T) src : objectMapper.readValue(src,clazz);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将json转换成对象TypeReference
     * @param src
     * @param typeReference
     * @param <T>
     * @return
     */
    public static <T>T jsonToObject(String src, TypeReference<T> typeReference){
        if(StringUtils.isEmpty(src) || typeReference == null){
            return null;
        }
        try {
            return (T)(typeReference.getType().equals(String.class) ? src : objectMapper.readValue(src, typeReference));
        } catch (Exception e) {
        	logger.warn("Parse Json to Object error",e);
            e.printStackTrace();
            return null;
        }
    }
    
    public static JSONObject objectToJSONObject(Object obj) {
 		JSONObject jsonObject = JSONObject.fromObject(obj);
 		return jsonObject;
 	}
    
    /**
     *  @description json转换前map封装
     * */
    public static Map<String, Object>  outMap(String state,String message,Object o){
		Map<String, Object>  map=new HashMap<String, Object>();
		map.put("statecode", state);
		map.put("message", message);
		if(null != o){
			map.put("result", o);
		}
		return map;
	}
    /**
     *  @description json转换前map封装
     * */
    public static Map<String, Object>  outMap(String state,String message){
		Map<String, Object>  map=new HashMap<String, Object>();
		map.put("statecode", state);
		map.put("message", message);
		return map;
	}
}
