package com.define.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.define.entity.User;

public interface UserDao extends JpaRepository<User, String>{
	@Query(value="select * from user where identity =?1 and trade_status=3",nativeQuery=true)
	public List<User> findUserByIdentity(String Identity);
	public User findUserByTradeId(String tradeId);
	public User findUserByIdentityOrderByCurTimeAsc(String Identity);
}
