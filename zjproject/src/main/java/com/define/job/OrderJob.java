package com.define.job;

import java.util.List;
import java.util.Set;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.define.entity.User;
import com.define.service.UserService;

@Component
@DisallowConcurrentExecution
public class OrderJob implements Job {
	@Autowired
	private UserService service;

	private String tradeId;

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		User user = service.findUserByTradeId(tradeId);
		if (user.getTradeStatus() != null && user.getTradeStatus() == 2) {
			user.setTradeStatus(5);
			service.saveUser(user);
		}
		try {
			Scheduler scheduler = context.getScheduler();
			String schedulerName = scheduler.getSchedulerName();
			Set<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(tradeId));
			for (JobKey jobKey : jobKeys) {
				scheduler.unscheduleJob(TriggerKey.triggerKey(tradeId));
				scheduler.deleteJob(jobKey);
				scheduler.shutdown();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
