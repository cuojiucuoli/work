package com.define.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

/**
 * 下订单的人
 * 
 * @author Administrator
 *
 */
@Entity
public class User {
	@Id
	@GeneratedValue(generator = "userGenerator")
	@GenericGenerator(name = "userGenerator", strategy = "uuid")
	private String id;
	private String tradeId;
	private Integer tradeStatus; //1 新建 2已锁票 3已售票 5已取消 6确认失败 11确认中
	private String identity;
	private Integer timeout;
	private Long curTime;
	private String noNum;
	private Integer isPay;
	private Double poundage;
	
	
	public Integer getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public Integer getIsPay() {
		return isPay;
	}

	public void setIsPay(Integer isPay) {
		this.isPay = isPay;
	}

	public Double getPoundage() {
		return poundage;
	}

	public void setPoundage(Double poundage) {
		this.poundage = poundage;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}

	public String getNoNum() {
		return noNum;
	}
	
	public void setNoNum(String noNum) {
		this.noNum = noNum;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	
	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public Long getCurTime() {
		return curTime;
	}

	public void setCurTime(Long curTime) {
		this.curTime = curTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTradeId() {
		return tradeId;
	}

	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	

}
