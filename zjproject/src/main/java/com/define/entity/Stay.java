package com.define.entity;

public class Stay {

	private String name;
	private String depart_time;
	private int is_up;
	private int is_down;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepart_time() {
		return depart_time;
	}
	public void setDepart_time(String depart_time) {
		this.depart_time = depart_time;
	}
	public int getIs_up() {
		return is_up;
	}
	public void setIs_up(int is_up) {
		this.is_up = is_up;
	}
	public int getIs_down() {
		return is_down;
	}
	public void setIs_down(int is_down) {
		this.is_down = is_down;
	}
	
	
}
