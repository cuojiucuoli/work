package com.define.entity;

import java.util.List;

public class Coach {

	private String id;
	private String number;
	private String name;
	private String start;
	private String dest;
	private String depart_time;
	private List<Price> prices;
	private String vehicle;
	private String oper_type;
	private int remain_seat;
	private int millage;
	private String take_explain;
	private String refund_explain;
	private List<Stay> stays;
	private int classify;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	public String getDepart_time() {
		return depart_time;
	}
	public void setDepart_time(String depart_time) {
		this.depart_time = depart_time;
	}

	public List<Price> getPrices() {
		return prices;
	}
	public void setPrices(List<Price> prices) {
		this.prices = prices;
	}
	public String getVehicle() {
		return vehicle;
	}
	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}
	public String getOper_type() {
		return oper_type;
	}
	public void setOper_type(String oper_type) {
		this.oper_type = oper_type;
	}
	public int getRemain_seat() {
		return remain_seat;
	}
	public void setRemain_seat(int remain_seat) {
		this.remain_seat = remain_seat;
	}
	public int getMillage() {
		return millage;
	}
	public void setMillage(int millage) {
		this.millage = millage;
	}
	public String getTake_explain() {
		return take_explain;
	}
	public void setTake_explain(String take_explain) {
		this.take_explain = take_explain;
	}
	public String getRefund_explain() {
		return refund_explain;
	}
	public void setRefund_explain(String refund_explain) {
		this.refund_explain = refund_explain;
	}
	
	public List<Stay> getStays() {
		return stays;
	}
	public void setStays(List<Stay> stays) {
		this.stays = stays;
	}
	public int getClassify() {
		return classify;
	}
	public void setClassify(int classify) {
		this.classify = classify;
	}
	
	
}
