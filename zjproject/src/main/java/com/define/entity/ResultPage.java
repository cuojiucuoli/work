package com.define.entity;

import java.util.List;

public class ResultPage<E> {

	private List<E> data;
	private int count;
	private int page_count;
	private int curr_page;
	public List<E> getData() {
		return data;
	}
	public void setData(List<E> data) {
		this.data = data;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getPage_count() {
		return page_count;
	}
	public void setPage_count(int page_count) {
		this.page_count = page_count;
	}
	public int getCurr_page() {
		return curr_page;
	}
	public void setCurr_page(int curr_page) {
		this.curr_page = curr_page;
	}
	
	
	
}
