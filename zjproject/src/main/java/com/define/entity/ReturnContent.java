package com.define.entity;

public class ReturnContent<T> {

	private String code;
	private String msg;
	private ResultPage content;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public ResultPage getContent() {
		return content;
	}
	public void setContent(ResultPage content) {
		this.content = content;
	}
	
	
}
