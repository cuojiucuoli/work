package com.define.controller;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.define.common.CommonUtil;
import com.define.common.JobUtil;
import com.define.common.JsonUtil;
import com.define.entity.ResultPage;
import com.define.entity.ReturnContent;
import com.define.entity.Ticket;
import com.define.entity.Trade;
import com.define.entity.User;
import com.define.job.OrderJob;
import com.define.service.UserService;

import net.sf.json.JSONObject;

@RestController
public class PassengerTrafficController {

	@Autowired
	JobUtil jobUtil;
	@Autowired
	UserService service;

	private static Logger log = LoggerFactory.getLogger(PassengerTrafficController.class);

	/**
	 * 
	 * @param source {"start":"Y"}
	 * @return
	 */
	@PostMapping("/findStationList")
	public Map<String, Object> findStationList(String source) {
		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			String sign = CommonUtil.generateSign(map);
			String content = CommonUtil.HttpClients("get_start_dests", sign, map);
			return JsonUtil.outMap("1", "请求成功", content);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("发生了些错误", e);
			return JsonUtil.outMap("-1", e.getMessage());
		}

	}

	/**
	 * 
	 * @param source {"start":"N","dest":"N","date":"N yyyyMMdd","begin_hour":"Y
	 *               0","end_hour":"Y 24","curr_page":"Y 1"}
	 * @return content {}
	 */
	@PostMapping("/findCoachList")
	@SuppressWarnings("all")
	public Map<String, Object> findCoachList(@RequestBody String source) {
		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			String sign = CommonUtil.generateSign(map);
			String content = CommonUtil.HttpClients("search_coach", sign, map);
			ReturnContent<ResultPage> conten = new ReturnContent<ResultPage>();
			ReturnContent jsonToObject = JsonUtil.jsonToObject(content, conten.getClass());
			ResultPage pageResult = jsonToObject.getContent();
			return new JsonUtil().outMap("1", "请求成功", pageResult);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("发生了些错误", e);
			return new JsonUtil().outMap("-1", "车位查询失败");
		}
	}

	/**
	 * 
	 * @param source {"coach_id":"N","depart_time":"N","ent_name":"N","ent_phone":"N",
	 *               "ticket_passengers":"[{"price_type":"N","price":"N","psg_name":"N","psg_cert_type":"int",psg_cert_no:int,free_child:int}]"}
	 * @return content {}
	 */
	@PostMapping("/dolock")
	@SuppressWarnings("all")
	public Map<String, Object> dolock(@RequestBody String source) {
		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			String identity = (String)map.get("identity");
			User pre_user = service.findUserByIdentityOrderByCurrentTimeAsc(identity);
			if (pre_user != null && pre_user.getTradeStatus() == 2) {
				return JsonUtil.outMap("-1", "请忽重复预定车票");
			}
			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			/*map.put("trade_id", "aaaaaa");*/
			
			map.remove("identity");
			map.put("trade_id", uuid);
			String sign = CommonUtil.generateSign(map);	
			String msg = CommonUtil.HttpClients("lock", sign, map);
			Map<String, Object> jsontoMap = JsonUtil.jsontoMap(msg);
			Map tradeMap = null;
			if (null != jsontoMap.get("content")) {
					tradeMap = (HashMap) jsontoMap.get("content");
					User user = new User();
					user.setTradeId(uuid);
					user.setTradeStatus((Integer) tradeMap.get("state"));
					user.setIdentity(identity);
					user.setTimeout((Integer) tradeMap.get("timeout") / 60);
					user.setCurTime(System.currentTimeMillis());
					service.saveUser(user);
					jobUtil.invokeJob(user.getTradeId(), user.getTimeout());
					String tradeStr = JsonUtil.objectToJSONObject(tradeMap).toString();
					Trade trades = JsonUtil.jsonToObject(tradeStr, Trade.class);
					return new JsonUtil().outMap("1", "预订成功", trades);
			} else {
				return new JsonUtil().outMap("-1", "服务器请求不成功,车票锁定失败"+jsontoMap.get("msg"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("发生了些错误", e);
			return new JsonUtil().outMap("-1", "服务器请求不成功,车票锁定失败");
		}
	}

	@PostMapping("/dounlock")
	@SuppressWarnings("all")
	public Map<String, Object> dounlock(String identity) {
		
		try {
			User user = service.findUserByIdentityOrderByCurrentTimeAsc(identity);
			if (null != user && user.getTradeStatus() != 5) {

				Map<String, Object> strMap = new HashMap<String, Object>();
				strMap.put("trade_id", user.getTradeId());
				String source = strMap.toString();
				Map<String, Object> map = JsonUtil.jsontoMap(source);
				String tradeId = (String) map.get("tradeId");
				String sign = CommonUtil.generateSign(map);
				String msg = CommonUtil.HttpClients("unlock", sign, map);
				Map<String, Object> map1 = JsonUtil.jsontoMap(msg);
				Map map2 = (Map) map1.get("content");
				if (null != map2 && map2.size() > 0) {
					String jsonStr = JsonUtil.objectToJSONObject(map2).toString();
					Trade trade = JsonUtil.jsonToObject(jsonStr, Trade.class);
					User user1 = service.findUserByTradeId(user.getTradeId());
					user1.setTradeStatus(trade.getState());
					service.saveUser(user1);
					return JsonUtil.outMap("1", "已经取消订票");
				} else {
					return JsonUtil.outMap("-1", "取消订票失败");
				}
			} else {
				return JsonUtil.outMap("-1", "取消失败,交易已取消");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return JsonUtil.outMap("-1", "订票状态异常");
		}

	}

	@PostMapping("/doquery")
	@SuppressWarnings("all")
	public Map<String, Object> query(String identity) {
		User user = service.findUserByIdentityOrderByCurrentTimeAsc(identity);
		String tradeId = user.getTradeId();
		if (null != user && tradeId != null) {
			Map<String, Object> strMap = new HashMap<String, Object>();
			strMap.put("trade_id", user.getTradeId());
			JSONObject object = JsonUtil.objectToJSONObject(strMap);
			String source = object.toString();
			try {
				Map<String, Object> map = JsonUtil.jsontoMap(source);
				String sign = CommonUtil.generateSign(map);
				String msg = CommonUtil.HttpClients("query", sign, map);
				Map<String, Object> mapTrade = JsonUtil.jsontoMap(msg);
				Map contenMap = (Map) mapTrade.get("content");
				if (null != contenMap && contenMap.size() > 0) {
					String jsonString = JsonUtil.objectToJSONObject(contenMap).toString();
					Trade trade = JsonUtil.jsonToObject(jsonString, Trade.class);
					return JsonUtil.outMap("1", "查询成功", trade);
				} else {
					return JsonUtil.outMap("-1", "查询失败");
				}
			} catch (Exception e) {
				e.printStackTrace();
				return JsonUtil.outMap("-1", "查询失败");
			}
		}
		return JsonUtil.outMap("-1", "查询失败");
	}

	@PostMapping("/docommit")
	@SuppressWarnings("all")
	public Map<String, Object> query(String source, String identity) {
		User user = service.findUserByIdentityOrderByCurrentTimeAsc(identity);
		Integer state = user.getTradeStatus();
		String tradeId = user.getTradeId();
		if (null == state || state != 2) {
			return JsonUtil.outMap("-1", "订票超时或订票信息状态错误");
		}

		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			map.put("trade_id", tradeId);
			String sign = CommonUtil.generateSign(map);
			String msg = CommonUtil.HttpClients("commit", sign, map);
			Map<String, Object> map1 = JsonUtil.jsontoMap(msg);
			Map<String, Object> map2 = (Map<String, Object>) map1.get("content");
			if (null != map2 && map2.size() > 0) {
				Integer tradeState = (Integer) map2.get("state");
				if (null != tradeState && tradeState == 3) {
					List list = (List) map2.get("tickets");
					Ticket ticket = (Ticket) list.get(0);
					user.setNoNum(ticket.getNo());
					service.saveUser(user);
					return JsonUtil.outMap("1", "售票成功", map2);
				} else {

				}
			} else {

			}
		} catch (Exception e) {
			e.printStackTrace();
			return JsonUtil.outMap("0", "网络异常");
		}
		return null;
	}

	@PostMapping("/doquery_poundage")
	@SuppressWarnings("all")
	public Map<String, Object> doquery_poundage(String source) {
		Map<String, Object> sourceMap = null;
		try {
			sourceMap = JsonUtil.jsontoMap(source);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		String tradeId = (String) sourceMap.get("trade_id");
		User user = service.findUserByTradeId(tradeId);
		String no = user.getNoNum();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("trade_id", tradeId);
		map.put("ticket_no", no);
		String sign = CommonUtil.generateSign(map);
		String msg;
		try {
			msg = CommonUtil.HttpClients("query_poundage", sign, map);
			Map<String, Object> jsonMap = JsonUtil.jsontoMap(msg);
			Map map1 = (Map) jsonMap.get("content");
			if (null != map1 && map1.size() > 0) {

				return JsonUtil.outMap("1", "查询成功", map1);
			} else {

				return JsonUtil.outMap("-1", "查询失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return JsonUtil.outMap("-1", "查询失败");
		}

	}

	@PostMapping("/refund")
	@SuppressWarnings("all")
	public Map<String, Object> refund(String source) {
		try {
			Map<String, Object> map = JsonUtil.jsontoMap(source);
			String sign = CommonUtil.generateSign(map);
			String msg = CommonUtil.HttpClients("refund", sign, map);

		} catch (Exception e) {
			e.printStackTrace();
			return JsonUtil.outMap("-1", "退票失败");
		}
		return null;
	}

	/**
	 * 退票前 查询用户已订购信息
	 */
	public Map<String, Object> findOrderInfo(String identity) {
		List<User> list = service.findUserByIdentity(identity);
		if (list == null) {
			return JsonUtil.outMap("-1", "信息查询不到");
		}
		return JsonUtil.outMap("1", "查询成功", list);
	}
}