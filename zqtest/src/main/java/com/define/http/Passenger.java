package com.define.http;

public class Passenger {

	private String name;
	private String cert_no;
	private int cert_type;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCert_no() {
		return cert_no;
	}
	public void setCert_no(String cert_no) {
		this.cert_no = cert_no;
	}
	public int getCert_type() {
		return cert_type;
	}
	public void setCert_type(int cert_type) {
		this.cert_type = cert_type;
	}
	
	
}
