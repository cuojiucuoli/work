package com.define.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.DigestUtils;

import com.alibaba.fastjson.JSONObject;
import com.define.http.Passenger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Query {
	
	/**
	 * {"coach_id":"lockCheciBanci$06cf17bf-039108ca6c70-02da-20180914,08ca1d2d-1a18,08ca1d2d-1b18","ent_phone":32151243,"depart_time":"201809142030","trade_id":"sdsfdf","ticket_passengers":[{"free_child":0,"psg_cert_no":"123","price":1.0,"psg_cert_type":2,"psg_name":"李四","price_type":1}],"ent_name":"李四"}
	 * 锁定座位
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	public void HttpClients() throws ClientProtocolException, IOException {
		CloseableHttpClient httpClient = org.apache.http.impl.client.HttpClients.createDefault();
		HttpPost mj = new HttpPost("https://lwx.navboy.com/api/sale/0855a353-01e1/unlock?account=open~op&sign=b2fc24c965a4f514fbd21639bd8ec0d2");
		mj.setHeader("Content-Type","application/json");
		mj.setHeader("charset","charset=utf-8");


		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("trade_id", "2s1dsfdf"); 
	
		String js = JSONObject.toJSONString(map);
		System.out.println(js);
		StringEntity se = new StringEntity(js,"utf-8");
	    se.setContentType("text/json;charset=utf-8");
	    mj.setEntity(se);
		HttpResponse response = httpClient.execute(mj);
		HttpEntity entity = response.getEntity();
		String string = EntityUtils.toString(entity);
		System.out.println(string);
		
		
	}
	
	/**
	 * 锁定座位
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Test
	public void getHa() {
		String password = "e10adc3949ba59abbe56e057f20f883e";	
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("trade_id", "2s1dsfdf"); 
		
		
		String json = JSONObject.toJSONString(map);
		System.out.println(json);
		String A = password+json;
		try {
			String md5DigestAsHex = DigestUtils.md5DigestAsHex(A.getBytes("UTF-8"));
			System.out.println(md5DigestAsHex);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void HAHA() {
		long currentTimeMillis = System.currentTimeMillis();
		String time = currentTimeMillis+"";
	}
}
